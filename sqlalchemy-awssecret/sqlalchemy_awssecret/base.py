"""
Support for reading an AWS secret, forward to the correct dialect and pass
all required connection parameters based on information from the secret .


"""
from sqlalchemy import event
from sqlalchemy.dialects.postgresql.psycopg2 import PGDialect_psycopg2
from sqlalchemy.engine import default
from sqlalchemy_redshift.dialect import RedshiftDialect

from sqlalchemy_awssecret.utils.secretsmanager import SecretsManager

SECRET_PARAMETER_KEY = 'secret='
POSTGRES_ENGINE_NAME = 'postgres'
REDSHIFT_ENGINE_NAME = 'redshift+psycopg2'


class AwsSecretDialect(default.DefaultDialect):

    secrets_manager = SecretsManager()

    @classmethod
    def get_dialect_cls(cls, url):
        if SECRET_PARAMETER_KEY not in str(url):
            raise ValueError(f"Provided awsrds url is not valid, {SECRET_PARAMETER_KEY} not found")

        secret_input = str(url).split(SECRET_PARAMETER_KEY)[1]
        secret = cls.secrets_manager.read_secret(secret_input)

        engine = secret['engine']
        if engine == POSTGRES_ENGINE_NAME:
            processed_dialect = PGDialect_psycopg2
        elif engine == REDSHIFT_ENGINE_NAME:
            processed_dialect = RedshiftDialect
        else:
            raise ValueError(f"Provided engine is invalid. Should be one one of "
                             f"[{POSTGRES_ENGINE_NAME}, {REDSHIFT_ENGINE_NAME}]")

        @event.listens_for(processed_dialect, 'do_connect')
        def receive_do_connect(dialect, conn_rec, cargs, cparams):
            cparams = {
                'host': secret['host'],
                'database': secret['dbname'],
                'user': secret['username'],
                'password': secret['password'],
                'port': int(secret['port'])
            }
            return dialect.connect(*cargs, **cparams)

        return processed_dialect
