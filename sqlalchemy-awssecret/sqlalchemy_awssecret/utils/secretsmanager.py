import base64
import json
from typing import Dict, Union

from botocore.exceptions import ClientError, NoCredentialsError

from sqlalchemy_awssecret.utils.giboto import make_client


class SecretsManager(object):
    def __init__(self) -> None:
        self.client = make_client(service_name='secretsmanager')

    def read_secret(self, secret: Union[str, bytes]) -> Dict[str, str]:
        try:
            get_secret_value_response = self.client.get_secret_value(SecretId=secret)
            if 'SecretString' in get_secret_value_response:
                secret = get_secret_value_response['SecretString']
            else:
                secret = base64.b64decode(get_secret_value_response['SecretBinary'])
            decoded_secret: Dict[str, str] = json.loads(secret)
            return decoded_secret
        except ClientError:
            raise SystemError('Could not retrieve secret')
        except NoCredentialsError:
            raise SystemError('Could not retrieve secret, bad credentials')
