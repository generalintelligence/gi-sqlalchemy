import os
import time
from typing import Any, Dict, Optional

import boto3


MAIN_ACCOUNT_ID = '829264618005'
PROD_ACCOUNT_ID = '540406626476'
PROD_ACCOUNT_ROLE_ARN = 'arn:aws:iam::829264618005:role/giProdToMainInvoke'


def whoami() -> None:
    boto_session = boto3.session.Session()
    ident = boto_session.client('sts').get_caller_identity()
    print(f"ident={ident}")


def make_environ() -> os._Environ:
    boto_session = boto3.session.Session()
    current_account_id = boto_session.client('sts').get_caller_identity().get('Account')
    if current_account_id == MAIN_ACCOUNT_ID:
        return os.environ
    result = os.environ
    result.update({'AWS_CONFIG_FILE': '/app/configs/prod_aws_config'})
    return result


def make_aws_cli_start() -> str:
    boto_session = boto3.session.Session()
    current_account_id = boto_session.client('sts').get_caller_identity().get('Account')
    if current_account_id == MAIN_ACCOUNT_ID:
        return 'aws'
    return 'aws --profile prod'


def make_client(service_name: str, from_main: bool = False, **kwargs: Any) -> boto3.client:
    boto_session = boto3.session.Session()
    kwargs.update(make_client_override_auth_params(boto_session, from_main))
    return boto_session.client(
        service_name=service_name,
        **kwargs
    )


def make_client_override_auth_params(
    boto_session: Optional[boto3.session.Session] = None,
    from_main: bool = False
) -> Dict[str, str]:
    if not from_main:
        return {}

    if not boto_session:
        boto_session = boto3.session.Session()
    current_account_id = boto_session.client('sts').get_caller_identity().get('Account')
    if current_account_id == MAIN_ACCOUNT_ID:
        return {}

    assumed_role_object = boto_session.client('sts').assume_role(
        RoleArn=PROD_ACCOUNT_ROLE_ARN,
        RoleSessionName=f'gi-boto-script-{time.time()}',
    )
    credentials = assumed_role_object['Credentials']
    credentials_to_use = {
        'aws_access_key_id': 'AccessKeyId',
        'aws_secret_access_key': 'SecretAccessKey',
        'aws_session_token': 'SessionToken'
    }
    return {k: credentials[credentials_to_use[k]] for k in credentials_to_use.keys()}
