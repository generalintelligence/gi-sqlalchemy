import os
import re

from setuptools import setup

v = open(os.path.join(os.path.dirname(__file__), 'sqlalchemy_awssecret', '__init__.py'))
VERSION = re.compile(r".*__version__ = '(.*?)'", re.S).match(v.read()).group(1)
v.close()

readme = os.path.join(os.path.dirname(__file__), 'README.md')


setup(name='sqlalchemy_awssecret',
      version=VERSION,
      description="AWS Secret Dialect for SQLAlchemy",
      long_description=open(readme).read(),
      classifiers=[
      'Development Status :: Alpha',
      'Environment :: Console',
      'Intended Audience :: Developers',
      'Programming Language :: Python :: 3',
      'Topic :: Database :: Front-Ends',
      ],
      keywords='SQLAlchemy AWS Secret',
      author='Cristian Calugaru & Petru Balau',
      author_email='cristi@g18e.com, petru@g18e.com',
      packages=['sqlalchemy_awssecret'],
      include_package_data=True,
      entry_points={
         'sqlalchemy.dialects': [
              'awsrds = sqlalchemy_awssecret.base:AwsSecretDialect'
              ]
        }
)
